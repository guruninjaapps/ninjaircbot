NinjaIrcBot
==============

Bot para IRC com alguns comandos de previsão do tempo, ping, piadas, tradução de frases, número de resultados no google pelo termo buscado, desenvolvido por <a href="http://ninjaapps.com.br">Ninja Apps</a> em tempo livre. 
<br/><br/><br/>
<b>Requerimentos:</b><br/>
<ul>
<li>Python 3.5 ou superior</li>
</ul><br/><br/>
<b>Instalação:</b><br/>
<ul>
<li>Instale o python versão 3.5 ou superior</li>
<li>Instalando as dependências: $ pip install -r requirements.txt</li>
<li>Configure o servidor, canal e nick do bot no arquivo config.py</li>
<li>Executando a aplicação: $ python bot.py</li>
</ul>
<br/><br/>
<b>Uso:</b><br/>
<ul>
<li>!ajuda - lista ajuda dos comandos existentes</li>
<li>!ping - exibe mensagem escrita pong</li>
<li>!google *frase* - mostra quandos resultados encontrados no google com a frase buscada</li>
<li>!tempo *cidade* - mostra previsão do tempo atual da cidade e/ou termo buscado usando Yahoo Weather</li>
<li>!piada - exibe uma piada aleatória</li>
<li>!traduzir *de_idioma*|*para_idioma*|*frase* comando traduz frases de um idioma a outro utilizando pytranslate, não utilize consecutivamente, o serviço traz instabilidades se detectar alto tráfego</li>
<li>!horoscopo *nome* - substitua o nome pelo nome do horóscopo sem qualquer acentuação</li>
<li>!duelo *nick* vs *nick* - duelo por número de resultados no google, quem será o vencedor?</li>

</ul><br/><br/>
<br/><br/>
