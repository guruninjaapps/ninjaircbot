import socket
import sys
from weather import Weather, Unit
import urllib
import json
from datetime import date, datetime
import requests
import argparse
from bs4 import BeautifulSoup
import pycurl
from io import BytesIO
from urllib.parse import urlencode, quote_plus
import time
from config import server, channel, botnick
from translate import translator
import feedparser
import re
from unicodedata import normalize

class Bot():
    def removerAcentos(self,txt):
        return normalize('NFKD', txt).encode('ASCII', 'ignore').decode('ASCII')

    def cleanhtml(self,raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    def iniciar(self):
        weather = Weather(unit=Unit.CELSIUS)

        previsao = {
            'tornado': 'Tornado',
            'tropical storm': 'Tempestade tropical',
            'hurricane': 'Furacão',
            'severe thunderstorms': 'Tempestade severa',
            'thunderstorms': 'Trovoadas',
            'mixed rain and snow':  'Chuva e neve',
            'mixed rain and sleet':  'Chuva e granizo fino',
            'mixed snow and sleet':  'Neve e granizo fino',
            'freezing drizzle':  'Garoa gélida',
            'drizzle':  'Garoa',
            'mostly sunny': 'Ensolarado com nuvens',
            'freezing rain': 'Chuva gélida',
            'showers': 'Chuva',
            'snow flurries': 'Neve em flocos finos',
            'light snow showers': 'Leve precipitação de neve',
            'blowing snow': 'Ventos com neve',
            'snow': 'Neve',
            'hail': 'Chuva de granizo',
            'sleet': 'Pouco granizo',
            'dust': 'Pó em suspensão',
            'foggy': 'Neblina',
            'haze': 'Névoa seca',
            'smoky': 'Enfumaçado',
            'blustery': 'Vendaval',
            'windy': 'Ventando',
            'cold': 'Frio',
            'cloudy': 'Nublado',
            'clear': 'Céu limpo',
            'sunny': 'Ensolarado',
            'fair': 'Tempo bom',
            'mixed rain and hail': 'Chuva e granizo',
            'hot': 'Quente',
            'isolated thunderstorms': 'Tempestades isoladas',
            'scattered thunderstorms': 'Tempestades esparsas',
            'scattered showers': 'Chuvas esparsas',
            'heavy snow': 'Nevasca',
            'scattered snow showers': 'Tempestades de neve esparsas',
            'heavy snow': 'Nevasca',
            'partly cloudy': 'Parcialmente nublado',
            'thundershowers': 'Chuva com trovoadas',
            'snow showers': 'Tempestade de neve',
            'isolated thundershowers': 'Relâmpagos e chuvas isoladas',
            'not available': 'Não disponível',
            'mostly cloudy': 'Sol com muitas nuvens'
            }

        irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        irc.connect((server, 6667))
        irc.send(("USER "+ botnick +" "+ botnick +" "+ botnick +" :This is a fun bot!\n").encode())
        irc.send(("NICK "+ botnick +"\n").encode())
        irc.send(("PRIVMSG nickserv :iNOOPE\r\n").encode())
        irc.send(("JOIN "+ channel +"\n").encode())

        while 1:
            text=irc.recv(2040)
            if len(text) == 0:
                irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
                irc.connect((server, 6667))
                irc.send(("USER "+ botnick +" "+ botnick +" "+ botnick +" :This is a fun bot!\n").encode())
                irc.send(("NICK "+ botnick +"\n").encode())
                irc.send(("PRIVMSG nickserv :iNOOPE\r\n").encode())
                irc.send(("JOIN "+ channel +"\n").encode())
                
            if text :
                if text.decode()[0:4] == 'PING':
                    irc.send(("PONG" + text.decode().split('PING')[1] + "\n").encode())
                if   text.decode().find('!google') != -1:
                    t = text.decode().split(':!google ')
                    buffer = BytesIO()
                    c = pycurl.Curl()
                    params = {'hl': 'pt','q': t[1]}
                    c.setopt(c.URL, 'http://www.google.com/search?'+ urlencode(params))
                    c.setopt(c.WRITEDATA, buffer)
                    c.perform()
                    c.close()
                    parsed_html = BeautifulSoup(buffer.getvalue(), 'html.parser')
                    irc.send(('PRIVMSG '+channel+' : '+parsed_html.body.find('div', attrs={'id':'resultStats'}).text+'\r\n').encode())
                elif text.decode().find('!duelo') != -1:
                    t = text.decode().split(':!duelo ')
                    buffer = BytesIO()
                    c = pycurl.Curl()
                    params = {'hl': 'pt','q': t[1].lower().split(' vs ')[0]}
                    c.setopt(c.URL, 'http://www.google.com/search?'+ urlencode(params))
                    c.setopt(c.WRITEDATA, buffer)
                    c.perform()
                    c.close()
                    parsed_html = BeautifulSoup(buffer.getvalue(), 'html.parser')
                    a = parsed_html.body.find('div', attrs={'id':'resultStats'}).text

                    buffer = BytesIO()
                    c = pycurl.Curl()
                    params = {'hl': 'pt','q': t[1].lower().split(' vs ')[1]}
                    c.setopt(c.URL, 'http://www.google.com/search?'+ urlencode(params))
                    c.setopt(c.WRITEDATA, buffer)
                    c.perform()
                    c.close()
                    parsed_html = BeautifulSoup(buffer.getvalue(), 'html.parser')
                    b = parsed_html.body.find('div', attrs={'id':'resultStats'}).text

                    a1 = float(a.replace('Aproximadamente ','').replace(' resultados','').replace('.',''))
                    b1 = float(b.replace('Aproximadamente ','').replace(' resultados','').replace('.',''))

                    vencedor = 'Ixi deu empate'
                    if a1>b1:
                        vencedor = t[1].lower().split(' vs ')[0]
                    elif b1>a1:
                        vencedor = t[1].lower().split(' vs ')[1]
                    irc.send(('PRIVMSG {} : {} {} VS {} {}. Vencedor deste duelo: {}.\r\n'.format(channel, t[1].lower().split(' vs ')[0].strip(), a.replace('Aproximadamente ','').strip(), t[1].lower().split(' vs ')[1].strip(), b.replace('Aproximadamente ','').strip(), vencedor.strip())).encode())
                elif text.decode().find(':!ping') !=-1:
                    t = text.decode().split(':!ping')
                    irc.send(('PRIVMSG '+channel+' :pong\r\n').encode())
                elif text.decode().find(':!tempo') !=-1:
                    hj = date.today()
                    t = text.decode().split(':!tempo')
                    to = t[1].strip()
                    location = weather.lookup_by_location(to)
                    forecasts = location.forecast
                    condition = location.condition
                    for forecast in forecasts:
                        data = datetime.strptime(forecast.date, '%d %b %Y')
                        if data.day == hj.day:
                            irc.send(('PRIVMSG '+channel+' :Previsão do tempo: '+previsao[forecast.text.lower()]+' - Máxima: '+forecast.high+'ºC - Miníma: '+forecast.low+'ºC\r\n').encode())
                            break
                elif text.decode().find(':!piada') !=-1:
                    buffer = BytesIO()
                    c = pycurl.Curl()
                    c.setopt(c.URL, 'https://us-central1-kivson.cloudfunctions.net/charada-aleatoria')
                    c.setopt(c.WRITEDATA, buffer)
                    c.perform()
                    c.close()
                    parsed_html = BeautifulSoup(buffer.getvalue(), 'html.parser')
                    irc.send(('PRIVMSG '+channel+' : '+self.removerAcentos(parsed_html.body.find('h1').text)+'   '+self.removerAcentos(parsed_html.body.find('p').text)+'\r\n').encode())
                elif text.decode().find(':!ajuda') !=-1:
                    irc.send(('PRIVMSG '+channel+' :Lista de Comandos (adicionar ! na frente):\r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :ping - responde com pong\r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :google <frase> - exibe número de resultados encontrados pelo google com a frase a seguir\r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :tempo <cidade> - exibe a previsão do tempo atual para a cidade ou cidade estado, com Yahoo Weather \r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :piada - exibe piada aleatória\r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :traduzir <de_idioma>|<para_idioma>|<frase> - traduz texto de um idioma para outro, para pt-br utilize somente pt, utilizando py-translate, repita o passo, as vezes, ocorrem instabilidades, não utilize de forma consecutiva \r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :horoscopo <nome> - substitua o nome pelo nome do horóscopo sem qualquer acentuação \r\n').encode())
                    time.sleep(2)
                    irc.send(('PRIVMSG '+channel+' :duelo <nick> vs <nick> - o vencedor desta batalha será quem possuir mais resultados no buscador google \r\n').encode())
                elif text.decode().find(':!traduzir') !=-1:
                    hj = date.today()
                    t = text.decode().split(':!traduzir ')
                    to = t[1].strip()
                    traduzir = to.split('|')
                    traducao = translator(traduzir[0], traduzir[1], traduzir[2])[0][0][0]
                    irc.send(('PRIVMSG '+channel+' :Tradução de '+traduzir[0]+' para '+traduzir[1]+': '+traducao+'\r\n').encode())
                elif text.decode().find(':!horoscopo') !=-1:
                    t = text.decode().split(':!horoscopo ')
                    feeds = feedparser.parse('http://pt.horoscopofree.com/rss/horoscopofree-pt.rss')
                    for entry in feeds.entries:
                        if str(self.removerAcentos(entry.title.lower())).strip()==str(t[1]).strip():
                            irc.send(('PRIVMSG '+channel+' :Horóscopo '+entry.title+' -  '+self.cleanhtml(entry.summary.replace('Inscreve-te ao Horóscopo grástis em e-mail!',''))+'\r\n').encode())
                            break

bot = Bot()
while True:
    try:
        bot.iniciar()
    except:
        pass
    else:
        break
